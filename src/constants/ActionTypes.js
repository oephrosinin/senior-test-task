export const SORT = 'SORT';
export const FILTER = 'FILTER';
export const RECEIVE_ITEM = 'RECEIVE_ITEM';
export const RECEIVE_ITEMS = 'RECEIVE_ITEMS';
export const ADD_TO_FAVORITES = 'ADD_TO_FAVORITES';

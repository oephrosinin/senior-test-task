/**
 * Generates sort method
 * It needed to sort severalfields
 * @param args
 * @returns {Function}
 */
export function getSortMethod(...args) {
  return function(a, b){
    for(const x in args){
      let ax = a[args[x].substring(1)];
      let bx = b[args[x].substring(1)];
      let cx;

      ax = typeof ax === "string" ? ax.toLowerCase() : ax / 1;
      bx = typeof bx === "string" ? bx.toLowerCase() : bx / 1;

      if(args[x].substring(0,1) === '-'){
        cx = ax;
        ax = bx;
        bx = cx;
      }

      if(ax !== bx) {
        return ax < bx ? -1 : 1;
      }
    }
  }
}

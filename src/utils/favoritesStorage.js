// we could check storage with try, but now it's not necessary
const hasStorage = typeof(Storage) !== "undefined";

if (!hasStorage) {
  console.log('Sorry! No Web Storage support..');
}

/**
 * Add favorite
 * @param favorite
 * @returns {*}
 */
export const addOne = (favorite) => {
  if (!hasStorage) {
    return false;
  }

  const savedFavorites = JSON.parse((localStorage.getItem('favorites') || '[]'));
  savedFavorites.push(favorite);
  return localStorage.setItem('favorites', JSON.stringify(savedFavorites));
};

/**
 * Delete favorite
 * @param favorite
 * @returns {*}
 */
export const deleteOne = (favorite) => {
  if (!hasStorage) {
    return false;
  }

  const savedFavorites = JSON.parse((localStorage.getItem('favorites') || '[]'));
  const index = savedFavorites.indexOf(favorite);
  if (index > -1) {
    savedFavorites.splice(index, 1);
    return localStorage.setItem('favorites', JSON.stringify(savedFavorites));
  }

  return false;
};

/**
 * Get favorites id
 * @returns {*}
 */
export const getFavorites = () => {
  if (!hasStorage) {
    return [];
  }

  return JSON.parse(localStorage.getItem('favorites') || '[]');
};
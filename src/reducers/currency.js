import _ from 'lodash';
import * as FavoritesStorage from '../utils/favoritesStorage';
import { getSortMethod } from '../utils/advancedSort';

// Import constants
import {
  SORT,
  FILTER,
  RECEIVE_ITEM,
  RECEIVE_ITEMS,
  ADD_TO_FAVORITES,
} from '../constants/ActionTypes';

// Our initial state
const initialState = {
  items: [],
  visibleItems: [],
  sort: {
    field: 'assetName',
    type: 'asc',
  },
  filterValue: '',
};


/**
 * Get filtered and sorted items
 * @param state
 * @returns {*}
 */
const getItems = state => {
  const filterValue = state.filterValue.toLowerCase();
  const { sort } = state;
  // Get sort field depends on order type
  const sortField = sort.type === 'asc' ? `+${sort.field}` : `-${sort.field}`;

  const visibleItems = state.items
    .filter(({id, assetName, price, lastUpdate, type}) => {
      if (!filterValue) {
        return true;
      }
      return (
        id.toString().includes(filterValue) || assetName.toLowerCase().includes(filterValue) ||
        price.toString().includes(filterValue) || lastUpdate.toString().includes(filterValue) ||
        type.toLowerCase().includes(filterValue)
      );
    })
    .sort(getSortMethod('-favorite', sortField));

  return {
    ...state,
    filterValue,
    visibleItems,
  }
};

/**
 * Reducer's actions handle
 * @param state
 * @param action
 * @returns {*}
 */
export default function currency(state = initialState, action) {
  switch (action.type) {

    // Add item to favorites
    case ADD_TO_FAVORITES:
      const { id } = action;
      const itemIndex = state.items.findIndex((item) => item.id === id);

      if (state.items[itemIndex].favorite) {
        // Delete from storage
        FavoritesStorage.deleteOne(id);
        state.items[itemIndex].favorite = false;
      } else {
        // Add to store storage
        FavoritesStorage.addOne(id);
        state.items[itemIndex].favorite = true;
      }

      return getItems(state);

    // Sort name any column
    case SORT:
      const { field } = action;
      if (field === state.sort.field) {
        if (state.sort.type === 'asc') {
          state.sort.type = 'desc';
        } else {
          state.sort.type = 'asc';
        }
      } else {
        state.sort.type = 'asc';
        state.sort.field = field;
      }

      return getItems(state);

    // Filter columns
    case FILTER:
      state.filterValue = action.value;
      return getItems(state);

    // Handle receive all items
    case RECEIVE_ITEMS:
      const { items } = action;

      // If we don't have items, it's a first load
      // Also we can check for the favorites to apply
      if (!state.items.length) {
        const favorites = FavoritesStorage.getFavorites();
        items.map((item) => item.favorite = favorites.includes(item.id));
        state.items = items;
        return getItems(state);
      }

      // price and lastUpdate
      items.forEach(({id, price, lastUpdate}) => {
        const stateItem = _.find(state.items, { id });
        if (stateItem) {
          stateItem.price = price;
          stateItem.lastUpdate = lastUpdate;
        }
      });

      state.items = items;
      return getItems(state);

    default:
      return state
  }
}

import currency from './currency';

describe('reducers', () => {
  describe('currency', () => {
    const initialState = {
      items: [],
      currentItems: [],
      sort: {
        field: 'assetName',
        type: 'asc',
      },
      filter: '',
    };

    it('should provide the initial state', () => {
      expect(currency(undefined, {})).toEqual(initialState)
    });

    it('should handle RECEIVE_ITEMS action', () => {
      const payloadItems = [
        {
          id: 1,
          assetName: 'test',
          price: Math.random()*10,
          lastUpdate: Date.now(),
          type: 'test'
        },
        {
          id: 2,
          assetName: 'test1',
          price: Math.random()*10,
          lastUpdate: Date.now(),
          type: 'test1'
        }
      ];
      expect(currency(initialState, { type: 'RECEIVE_ITEMS', items: payloadItems })).toEqual({
        items: payloadItems,
        currentItems: payloadItems,
        sort: {
          field: 'assetName',
          type: 'asc',
        },
        filter: '',
      });
    });

  })
});

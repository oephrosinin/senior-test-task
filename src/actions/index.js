import * as types from '../constants/ActionTypes'

export const doSort = field => ({
  type: types.SORT,
  field,
});

export const filter = value => ({
  type: types.FILTER,
  value,
});

export const receiveItems = items => ({
  type: types.RECEIVE_ITEMS,
  items,
});

export const addToFavorites = id => ({
  type: types.ADD_TO_FAVORITES,
  id
});

// export const checkout = products => (dispatch, getState) => {
//   const { cart } = getState()
//
//   dispatch({
//     type: types.CHECKOUT_REQUEST
//   })
//   shop.buyProducts(products, () => {
//     dispatch({
//       type: types.CHECKOUT_SUCCESS,
//       cart
//     })
//     // Replace the line above with line below to rollback on failure:
//     // dispatch({ type: types.CHECKOUT_FAILURE, cart })
//   })
// }

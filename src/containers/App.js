import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ContentTable } from '../components/ContentTable/index';
import * as Actions from '../actions'
// import {receiveItem} from "../actions";

const App = ({currency, actions}) => {
  const { visibleItems, filterValue, sort } = currency;

  return (
    <div className='container'>
      <ContentTable
        items={visibleItems}
        filterValue={filterValue}
        sort={sort}
        {...actions} />
    </div>
  )
};

App.propTypes = {
  actions: PropTypes.object,
  items: PropTypes.array,
  filterValue: PropTypes.string,
  sort: PropTypes.object,
};

const mapStateToProps = ({currency}) => ({currency});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
});


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
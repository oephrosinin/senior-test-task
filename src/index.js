import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
// import thunk from 'redux-thunk';
import reducer from './reducers';
import { receiveItems } from './actions';
import App from './containers/App';
import { mock } from "./utils/currencyGenerator";

const store = createStore(reducer);


let items = [];
let interval = setInterval(() => {
  if (!items.length) {
    return;
  }
  store.dispatch(receiveItems(items));
  items = [];
}, 500);


mock.subscribe(val => {
  items.push(val);
});

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

import '../styles.scss';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { CurrencyItem } from "./Item";

const ID_FIELD = 'id';
const TYPE_FIELD = 'type';
const PRICE_FIELD = 'price';
const ASSET_FIELD = 'assetName';
const LAST_UPDATE_FIELD = 'lastUpdate';

export class ContentTable extends PureComponent {

  constructor(props) {
    super(props);
  }

  render() {
    const { sort, items, filterValue } = this.props;
    const { field, type } = sort;

    const getColumnClassname = (fieldName) => {
      return cx('sorting', {
        active: field === fieldName,
        down: type === 'desc',
      });
    };

    return (
      <div>
        <table style={{border: '1px solid lightgray'}}>
          <thead>
          <tr className="search">
            <th colSpan={5}>
              <input ref={input => this._input = input}
                     defaultValue={filterValue}
                     type="text"
                     placeholder="Search"
                     onChange={this._onSearchChange} />
            </th>
          </tr>
          <tr>
            <th>Number</th>
            <th className={getColumnClassname(ID_FIELD)} onClick={() => this._sort(ID_FIELD)}>Id</th>
            <th className={getColumnClassname(ASSET_FIELD)} onClick={() => this._sort(ASSET_FIELD)}>Asset Name</th>
            <th className={getColumnClassname(PRICE_FIELD)} onClick={() => this._sort(PRICE_FIELD)}>Price</th>
            <th className={getColumnClassname(TYPE_FIELD)} onClick={() => this._sort(TYPE_FIELD)}>Type</th>
            <th className={getColumnClassname(LAST_UPDATE_FIELD)} onClick={() => this._sort(LAST_UPDATE_FIELD)}>Last Update</th>
          </tr>
          </thead>
          <tbody>
          {items.map((item, index) => <CurrencyItem
            key={`${index}_${item.id}`}
            index={index}
            {...item}
            addToFavorites={this.props.addToFavorites} />)}
          </tbody>
        </table>
      </div>
    );
  }

  /**
   * Handle search change
   * @private
   */
  _onSearchChange = ({target}) => {
    const value = target.value.trim();
    if (value === this._previousValue) {
      return;
    }

    if (this._timer) {
      clearTimeout(this._timer);
    }

    this._timer = setTimeout(() => {
      this._previousValue = value;
      this.props.filter(value);
    }, 250);
  }

  /**
   * Sort by column name
   * @param column
   * @private
   */
  _sort = (column) => {
    this.props.doSort(column);
  }
}

ContentTable.propTypes = {
  sort: PropTypes.object,
  items: PropTypes.array,
  filterValue: PropTypes.string,
  currency: PropTypes.object,
  doSort: PropTypes.func,
  filter: PropTypes.func,
  addToFavorites: PropTypes.func,
};

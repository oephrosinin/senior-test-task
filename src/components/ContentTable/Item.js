import '../styles.scss';
import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

export const CurrencyItem = (props) => {
  const {
    id,
    type,
    price,
    favorite,
    lastUpdate,
    assetName,
    index,
    addToFavorites,
  } = props;

  return (
    <tr key={id} className={cx({favorite})}>
      <td>{index + 1}</td>
      <td>{id}</td>
      <td>{assetName}</td>
      <td>{price}</td>
      <td>{type}</td>
      <td>
        {lastUpdate}
        <span className="add-to-favorites" onClick={() => addToFavorites(id)}>★</span>
      </td>
    </tr>
  );
};

CurrencyItem.propTypes = {
  id: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
  favorite: PropTypes.bool.isRequired,
  assetName: PropTypes.string.isRequired,
  lastUpdate: PropTypes.number.isRequired,
  addToFavorites: PropTypes.func.isRequired,
};
